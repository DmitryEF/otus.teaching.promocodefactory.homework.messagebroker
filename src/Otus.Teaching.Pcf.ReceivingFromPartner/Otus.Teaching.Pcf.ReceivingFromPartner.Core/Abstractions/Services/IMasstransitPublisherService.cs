﻿namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Services
{
    using System;
    using System.Threading.Tasks;
    using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

    public interface IMasstransitPublisherService
    {
        Task PushGivePromocodeToCustomerMessage(PromoCode promoCode);

        Task PushNotifyAdminAboutPartnerManagerPromoCodeMessage(Guid partnerManagerId);
    }
}