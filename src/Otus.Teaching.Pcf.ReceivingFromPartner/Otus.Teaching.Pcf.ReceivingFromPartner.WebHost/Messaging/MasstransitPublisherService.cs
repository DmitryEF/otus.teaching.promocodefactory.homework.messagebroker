﻿namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Messaging
{
    using System;
    using System.Threading.Tasks;
    using MassTransit;
    using Otus.Teaching.Pcf.Messaging.CrossSystemContracts;
    using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Services;
    using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

    public class MasstransitPublisherService : IMasstransitPublisherService
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public MasstransitPublisherService(IPublishEndpoint publishEndpoint)
        {
            this._publishEndpoint = publishEndpoint;
        }

        public async Task PushGivePromocodeToCustomerMessage(PromoCode promoCode)
        {
            var message = new GivePromoCodeRequest()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PromoCodeId = promoCode.Id
            };

            await this._publishEndpoint.Publish(message);
        }

        public async Task PushNotifyAdminAboutPartnerManagerPromoCodeMessage(Guid partnerManagerId)
        {
            await this._publishEndpoint.Publish(new NotifyAdminAboutPartnerManagerPromoCodeMessage
            {
                PartnerManagerId = partnerManagerId
            });
        }
    }
}
