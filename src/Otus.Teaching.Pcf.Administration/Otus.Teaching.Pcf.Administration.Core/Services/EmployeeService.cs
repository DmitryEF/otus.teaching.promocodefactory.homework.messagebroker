﻿namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    using System;
    using System.Threading.Tasks;
    using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
    using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

    public class EmployeeService : IEmployeeService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeeService(IRepository<Employee> employeeRepository)
        {
            this._employeeRepository = employeeRepository;
        }

        public async Task<bool> UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await this._employeeRepository.GetByIdAsync(id);

            if (employee is null) 
                throw new ArgumentNullException($"Employee with id=\"{id}\" not exists!");

            employee.AppliedPromocodesCount++;

            await this._employeeRepository.UpdateAsync(employee);

            return true;

        }
    }
}
