﻿namespace Otus.Teaching.Pcf.Administration.WebHost.Messaging
{
    using System;
    using System.Threading.Tasks;
    using MassTransit;
    using Otus.Teaching.Pcf.Administration.Core.Services;
    using Otus.Teaching.Pcf.Messaging.CrossSystemContracts;

    public class NotifyAdminAboutPartnerManagerPromoCodeMessageConsumer 
        : IConsumer<NotifyAdminAboutPartnerManagerPromoCodeMessage>
    {
        private readonly IEmployeeService _employeeService;

        public NotifyAdminAboutPartnerManagerPromoCodeMessageConsumer(IEmployeeService employeeService)
        {
            this._employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<NotifyAdminAboutPartnerManagerPromoCodeMessage> context)
        {
            if (context?.Message?.PartnerManagerId == null)
            {
                throw new ArgumentException("PartnerManagerId == null!");
            }

            await this._employeeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId);
        }
    }
}
