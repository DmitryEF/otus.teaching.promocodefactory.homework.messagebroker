﻿namespace Otus.Teaching.Pcf.Messaging.CrossSystemContracts
{
    using System;

    public class NotifyAdminAboutPartnerManagerPromoCodeMessage
    {
        public Guid PartnerManagerId { get; set; }
    }
}
